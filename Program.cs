﻿using CodeCop.Core;
using CodeCop.Core.Fluent;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            Cop.AsFluent();
            InterceptAll<TestClass>();
            Cop.Intercept();

            var testclass = new TestClass();
            testclass.Name = "Duane";
            testclass.Greet("Joe");
            testclass.Name = "Joe";
            testclass.Greet("Duane");

            Console.ReadKey();
        }

        public static void InterceptAll<T>()
        {
            // If I comment out the constructor interception, then the method interception works just fine.
            // With both the constructor and the methods intercepted, then I get a null object reference.
            var type = typeof(T);
            foreach (var constructor in type.GetConstructors())
            {
                Console.WriteLine("Intercepting {0}", constructor);
                constructor.DecorateBefore(context =>
                {
                    Console.WriteLine("Pre {0}", context.InterceptedMethod);
                });
                constructor.DecorateAfter(context =>
                {
                    Console.WriteLine("Post {0}", context.InterceptedMethod);
                });
            }

            foreach (var method in type.GetMethods().Where(m => m.DeclaringType == type))
            {
                Console.WriteLine("Intercepting {0}", method);
                method.DecorateBefore(context =>
                {
                    try
                    {
                        Console.WriteLine("Pre {0}", context.InterceptedMethod.Name);
                    }
                    catch (Exception ex)
                    {
                        //throw;
                    }
                });
                method.DecorateAfter(context =>
                {
                    try
                    {
                        Console.WriteLine("Post {0}", context.InterceptedMethod.Name);
                    }
                    catch (Exception ex)
                    {
                        //throw;
                    }
                });
            }
        }
    }

    public class TestClass
    {
        public string Name { get; set; }
        public void Greet(string greetee)
        {
            Console.WriteLine("Hello {0} my name is {1}", greetee, Name);
        }
    }
}
